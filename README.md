# NeoGrafos

# Algoritmos


## Crear grafo con pesos:

```
CREATE (A:Nodo {nombre: 'A'}),
       (B:Nodo {nombre: 'B'}),
       (C:Nodo {nombre: 'C'}),
       (D:Nodo {nombre: 'D'}),
       (E:Nodo {nombre: 'E'})

CREATE (A)-[:CONEXION {costo: 1}]->(B),
       (A)-[:CONEXION {costo: 4}]->(C),
       (B)-[:CONEXION {costo: 2}]->(D),
       (C)-[:CONEXION {costo: 1}]->(D),
       (D)-[:CONEXION {costo: 2}]->(E),
       (B)-[:CONEXION {costo: 5}]->(E),
       (C)-[:CONEXION {costo: 3}]->(E)

```


## Verificar los Nodos:

```
MATCH (n:Nodo)
RETURN n
```

## Verificar relaciones:

```
MATCH (a:Nodo)-[r:CONEXION]->(b:Nodo)
RETURN a.nombre, type(r), r.costo, b.nombre

```

## Ejecutar el algoritmo de Dijkstra:

> Requiere de la extencion: apoc.algo.dijkstra
> Solo grafos ponderados

```
MATCH (start:Nodo {nombre: 'A'}), (end:Nodo {nombre: 'E'})
CALL apoc.algo.dijkstra(start, end, 'CONEXION', 'costo', 1.0/0.0, 1) YIELD path, weight
RETURN path, weight

```


## Busqueda basada en Relaciones

```
MATCH (s:Nodo {nombre:'A'}), (e:Nodo {nombre:'D'})
WITH s, e  
MATCH p = shortestPath((s)-[:CONEXION*]-(e))
RETURN p, reduce(cost=0, r in relationships(p) | cost + r.costo) AS totalCost
```




## Floyd-Warshal con APOC

> Requiere de la extencion: apoc.algo.floydWarshall
> Solo grafos ponderados

```
CALL apoc.algo.floydWarshall('Nodo', 'RELACION', 'costo', {write:true, writeProperty:'ruta'})

```



# Red Social


## Crear Nodo

```
CREATE (:Estudiante {nombre: 'Estudiante1', edad: 22, sexo: 'Hombre', musica: 'Rock', deporte: 'Fútbol'})

```


## Actualizar Nodo

```
MATCH (e:Estudiante {nombre: 'Estudiante1'})
SET e.edad = 23

```


## Borrar Nodo

```
MATCH (e:Estudiante {nombre: 'Estudiante1'})
DELETE e
```


## Filtar Nodo

```
MATCH (e:Estudiante) WHERE ID(e) = 46 RETURN e
```


## Busqueda en profundidad

```
MATCH (e1:Estudiante {musica: 'Rock'})-[*1..3]-(e2)
RETURN e2
```


## Busqueda en anchura, amigos de amigos

```
MATCH (e1:Estudiante {nombre: 'Estudiante1'})-[:AMIGO_DE*1..2]-(e2:Estudiante)
RETURN e2
```


## Busqueda en profundidad, amigos de amigos

```
MATCH (e1:Estudiante {nombre: 'Estudiante1'})-[:AMIGO_DE*1..3]-(e2:Estudiante)
RETURN e2

```



## Crear relaciones por gusto musical

```
MATCH (e1:Estudiante), (e2:Estudiante)
WHERE e1.musica = 'Rock' AND e2.musica = 'Rock' AND e1 <> e2
CREATE (e1)-[:GUSTA_ROCK]->(e2)
```


## Crear relaciones de amistad

```
MATCH (e1:Estudiante), (e2:Estudiante)
WHERE e1.musica = 'Pop' AND e2.musica = 'Pop' AND e1.edad < 30 AND e2.edad < 30 AND e1 <> e2
CREATE (e1)-[:AMIGOS]->(e2)
```


## Borrar Relacion

```
MATCH (e1:Estudiante {nombre: 'Estudiante1'})-[rel:AMIGO_DE]-(e2:Estudiante {nombre: 'Estudiante2'})
DELETE rel

```


## Borrar Relacion de amistad

```
MATCH (e1:Estudiante)-[rel:AMIGO_DE]-(e2:Estudiante)
WHERE NOT (e1.musica = 'Rock' AND e2.musica = 'Rock')
DELETE rel
```


## Borrar Grafo

```
MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r
```


# Con Python

```
# Ejemplo estructura de datos Universidad Konrad Lorenz
from py2neo import Graph

# Conecta con la base de datos Neo4j (ajusta los valores según tu configuración)
graph = Graph("bolt://localhost:7687", user="oscar", password="universidad")

# Definir una clase para el modelo de estudiante
class Student:
    def __init__(self, name, age, grade):
        self.name = name
        self.age = age
        self.grade = grade

# Función para crear un nuevo estudiante
def create_student(student):
    query = """
    CREATE (s:Student {name: $name, age: $age, grade: $grade})
    RETURN s
    """
    result = graph.run(query, name=student.name, age=student.age, grade=student.grade)
    return result


# Función para obtener un estudiante por nombre
def get_student_by_name(name):
    query = """
    MATCH (s:Student {name: $name})
    RETURN s
    """
    result = graph.run(query, name=name)
    return result

# Función para actualizar los atributos de un estudiante
def update_student(name, new_name, new_age, new_grade):
    query = """
    MATCH (s:Student {name: $name})
    SET s.name = $new_name, s.age = $new_age, s.grade = $new_grade
    RETURN s
    """
    result = graph.run(query, name=name, new_name=new_name, new_age=new_age, new_grade=new_grade)
    return result

# Función para eliminar un estudiante por nombre
def delete_student(name):
    query = """
    MATCH (s:Student {name: $name})
    DELETE s
    """
    graph.run(query, name=name)

if __name__ == "__main__":
    # Ejemplo de uso
    student1 = Student("Juan", 20, "A+")
    created_student = create_student(student1)
    print(f"Estudiante creado: {created_student}")

    student2 = Student("Maria", 22, "B")
    created_student = create_student(student2)
    print(f"Estudiante creado: {created_student}")

    found_student = get_student_by_name("Juan")
    print(f"Estudiante encontrado: {found_student}")

    update_student("Juan", "John", 21, "A")
    updated_student = get_student_by_name("John")
    print(f"Estudiante actualizado: {updated_student}")

    delete_student("John")
    print("Estudiante eliminado")



```











